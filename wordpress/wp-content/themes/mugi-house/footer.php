<footer class="c-footer">
	<div class="c-footer-navWrap">
		<div class="c-footer-info">
			<address class="c-footer-address">〒400ー1503　山梨県甲府市心経寺町490-1</address>
			<p class="c-footer-tel">055－266－3976</p>
		</div>
		<a href="<?php echo esc_url(home_url('/')); ?>" class="c-footer-navImg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/common/logo.png" alt="社会法人 友好福祉会 障害者支援施設 麦の家"></a>
		<ul class="c-footer-navList">
			<li class="c-footer-navItem"><a href="<?php echo esc_url(home_url('/')); ?>policy">大事にしていること</a></li>
			<li class="c-footer-navItem"><a href="<?php echo esc_url(home_url('/')); ?>life">麦の家の生活</a></li>
			<li class="c-footer-navItem"><a href="<?php echo esc_url(home_url('/')); ?>facility">施設紹介</a></li>
			<li class="c-footer-navItem"><a href="<?php echo esc_url(home_url('/')); ?>blog">麦の家ブログ</a></li>
			<li class="c-footer-navItem"><a href="<?php echo esc_url(home_url('/')); ?>recruit">採用情報</a></li>
			<li class="c-footer-navItem"><a href="<?php echo esc_url(home_url('/')); ?>document">保護者さま専用</a></li>
		</ul>
	</div>
	<div class="c-footer-copy">
		<div class="c-footer-copyInner">
			<a href="/" class="c-footer-link">友好福祉会トップページへ</a>
			<small class="c-footer-copyText">©copyright 社会福祉法人 友好福祉会 All Right Reserved.</small>
		</div>
	</div>
	<a href="#" class="c-footer-button js-footer-button"></a>
</footer><!-- /.c-footer -->

<?php wp_footer(); ?>
</body>
</html>

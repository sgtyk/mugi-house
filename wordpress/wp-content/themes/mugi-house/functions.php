<?php

/************************************************************************************************************************************************************************************************************************************************************************************************
 * -- Init
 ************************************************************************************************************************************************************************************************************************************************************************************************/


/**
 * 初期化.
 */
function init_mh() {
	add_theme_support('post-thumbnails');
}

add_action('init', 'init_mh');


/**
 * スタイルシートの読み込み.
 */
function load_mh_stylesheets() {
	wp_enqueue_style('shared', '/assets/css/index.css');
	wp_enqueue_style('document', '/assets/css/document.css');
}

add_action('wp_enqueue_scripts', 'load_mh_stylesheets');


/**
 * JSファイルの読み込み.
 */
function load_mh_scripts() {
	wp_deregister_script('jquery');

	wp_enqueue_script('vendor.dll', '/assets/js/vendor.dll.js');
	wp_enqueue_script('shared', '/assets/js/index.js');
}

add_action('wp_enqueue_scripts', 'load_mh_scripts');


 /**
 * カスタム画像サイズを追加.
 */
function add_mh_custom_img_size() {
	set_post_thumbnail_size(100, 100, true);

  add_image_size('blog', 610, 455, true);
}

add_action('after_setup_theme', 'add_mh_custom_img_size');


/**
* 概要（抜粋）の省略文字.
*/
function change_mh_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'change_mh_excerpt_more');


/**
* パスワード保護ページ
* https://tcd-theme.com/2016/08/password.html
*/

// タイトルの保護中を削除
function remove_protected($title) {
	return '%s';
}
add_filter('protected_title_format', 'remove_protected');

// テキストの変更
function my_password_form() {
  return
	'<div class="d-document-lock">
	<p class="d-document-lockText">
	保護者さま専用のページになります。<br>閲覧するにはパスワードを入力してください。
	</p><!-- /.d-document-lockText -->
	<section class="d-document-lockSection">
		<h3 class="d-document-lockTitle">保護者さま専用ページへログイン</h3>
		<form class="post_password" action="' . home_url() . '/wp-login.php?action=postpass" method="post">
			<input name="post_password" type="password" size="24" placeholder="パスワードを入力" class="d-document-lockInput">
			<button type="submit" name="Submit" class="d-document-lockBtn">
				ログイン
				<svg class="d-document-lockIcon" viewBox="0 0 16.25 27.96" xmlns="http://www.w3.org/2000/svg"><path d="m16.25 8.12c0-4.48-3.64-8.12-8.12-8.12s-8.13 3.64-8.13 8.12c0 3.89 2.72 7.13 6.37 7.93v11.91h6.68v-2.63h-3.16v-1.5h3.16v-2.63h-3.16v-5.14c3.64-.8 6.37-4.05 6.37-7.93zm-10.43-3.49c.38-1.27 1.72-1.99 2.99-1.62s1.99 1.72 1.62 2.99c-.38 1.27-1.72 1.99-2.99 1.62s-1.99-1.72-1.62-2.99z" fill="#fff"/></svg>
			</button>
		</form>
	</section><!-- /.d-document-lockSection -->
	</div><!-- /.d-document-lock -->';
}
add_filter('the_password_form', 'my_password_form');



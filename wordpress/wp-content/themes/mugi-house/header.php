<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>社会福祉法人 友好福祉会 障害者支援施設 麦の家</title>
  <?php wp_head(); ?>
</head>

<?php
$class_name = '';
$slug_name = basename(get_permalink());
if(is_front_page()) {
  $class_name = 'd-top fadeIn';
} else {
  switch ($slug_name) {
    case 'policy':
    $class_name = 'd-policy c-subPage';
    break;

    case 'life':
    $class_name = 'd-life c-subPage';
    break;

    case 'facility':
      $class_name = 'd-facility c-subPage';
      break;

    case 'blog':
      $class_name = 'd-blog c-subPage';
      break;

    case 'recruit':
      $class_name = 'd-recruit c-subPage';
      break;

    case 'document':
      $class_name = 'd-document c-subPage';
      break;

    default:
      $class_name = 'd-blog c-subPage';
      break;
  }
}
?>
<body class="<?php echo $class_name; ?>">
  <div class="c-header-wrap">
    <header class="c-header">
      <h1 class="c-header-logo">
        <a href="<?php echo esc_url(home_url('/')); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/common/logo.png" alt="社会法人 友好福祉会 障害者支援施設 麦の家"></a></h1>
      <div class="c-header-info">
        <address class="c-header-address">〒400ー1503　山梨県甲府市心経寺町490-1</address>
        <p class="c-header-tel">055－266－3976</p>
      </div>
    </header><!-- /.c-header -->

    <div class="c-header-buttonOpen js-header-buttonOpen">
      <span class="c-header-buttonOpenLine"></span>
      <span class="c-header-buttonOpenLine"></span>
      <span class="c-header-buttonOpenLine"></span>
      メニュー
    </div>
    <div class="c-header-buttonClose js-header-buttonClose">
      <span class="c-header-buttonCloseLine"></span>
      閉じる
    </div>

    <nav class="c-nav js-nav">
      <ul class="c-nav-list">
        <li class="c-nav-item <?php if (is_page('policy')) { echo 'is-active';} ?>"><a href="<?php echo esc_url(home_url('/')); ?>policy">大事にしていること</a></li>
        <li class="c-nav-item <?php if (is_page('life')) { echo 'is-active';} ?>"><a href="<?php echo esc_url(home_url('/')); ?>life">麦の家の生活</a></li>
        <li class="c-nav-item <?php if (is_page('facility')) { echo 'is-active';} ?>"><a href="<?php echo esc_url(home_url('/')); ?>facility">施設紹介</a></li>
        <li class="c-nav-item <?php if (is_page('blog')) { echo 'is-active';} ?>"><a href="<?php echo esc_url(home_url('/')); ?>blog">麦の家ブログ</a></li>
        <li class="c-nav-item <?php if (is_page('recruit')) { echo 'is-active';} ?>"><a href="<?php echo esc_url(home_url('/')); ?>recruit">採用情報</a></li>
        <li class="c-nav-item <?php if (is_page('document')) { echo 'is-active';} ?>"><a href="<?php echo esc_url(home_url('/')); ?>document">保護者さま専用</a></li>
      </ul>
    </nav><!-- /.c-nav -->
  </div><!-- /.headerWrap -->

<?php get_header(); ?>

  <ul class="d-top-slider js-top-slider">
    <li class="d-top-sliderItem d-top-sliderItem--1"></li>
    <li class="d-top-sliderItem d-top-sliderItem--2"></li>
    <li class="d-top-sliderItem d-top-sliderItem--3"></li>
  </ul><!-- /.d-top-slider -->

  <main class="d-top-main">
    <div class="d-top-inner">
      <div class="d-top-left">
        <section class="d-top-intro">
          <h2 class="d-top-introTitle">一人ひとりが満足できる施設をめざして。</h2>
          <p class="d-top-introText">麦の家は、甲府盆地の南端にある東京都の障害者支援施設。<br>はじまりは、昭和4 7 年。東京都の無認可保育園「小金井育児園」が重度の障害を持つ子どもたちを受け入れたことから。</p>
          <p class="d-top-introText">その子どもたちが成長し養護学校を卒業した後のことを心配した人たちによって計画され、山梨県に場所を得て開設されました。</p>
          <p class="d-top-introText">「麦は踏まれるほどに強くなる」。困難な障害を持つ人たちと、この施設に関わる人たちすべての心の支えとなるべく「麦の家」は開設されました。</p>
        </section><!-- /.d-top-intro -->

        <section class="d-top-news">
          <h2 class="d-top-newsTitle">新着情報とお知らせ</h2>
          <ol class="d-top-newsList">
            <?php
            $args = array(
              'posts_per_page' => 5,
            );
            $news_posts = get_posts($args);
            foreach ($news_posts as $post) : setup_postdata($post);
              $category = get_the_category();
              $cat_name = $category[0]->cat_name;
              $cat_slug = $category[0]->category_nicename;
            ?>
              <li class="d-top-newsItem">
                <?php if ($cat_name == 'blog'): ?>
                  <a href="<?php the_permalink(); ?>" class="d-top-newsAnchor">
                <?php else: ?>
                  <a href="<?php the_permalink(); ?>" class="d-top-newsAnchor">
                <?php endif; ?>
                  <date class="d-top-newsDate"><?php echo get_the_date('Y-m-d', $post->ID); ?></date>
                  <span class="d-top-newsTag d-top-newsTag--<?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span>
                  <p class="d-top-newsText"><?php the_title(); ?></p>
                </a>
              </li>
            <?php
            endforeach;
            wp_reset_postdata();
            ?>
          </ol><!-- /.d-top-newsList. -->
        </section><!-- /.d-top-news -->

        <div class="d-top-photo">
          <figure class="d-top-photoItem">
            <a href="<?php echo esc_url(home_url('/')); ?>policy">
              <img src="assets/images/top_img06.jpg" alt="大事にしていること">
              <figcaption class="d-top-photoText">大事にしていること</figcaption>
            </a>
          </figure>
          <figure class="d-top-photoItem">
            <a href="<?php echo esc_url(home_url('/')); ?>life">
              <img src="assets/images/top_img01.jpg" alt="麦の家の生活">
              <figcaption class="d-top-photoText">麦の家の生活</figcaption>
            </a>
          </figure>
          <figure class="d-top-photoItem">
            <a href="<?php echo esc_url(home_url('/')); ?>facility">
              <img src="assets/images/top_img02.jpg" alt="施設紹介">
              <figcaption class="d-top-photoText">施設紹介</figcaption>
            </a>
          </figure>
          <figure class="d-top-photoItem">
            <a href="<?php echo esc_url(home_url('/')); ?>blog">
              <img src="assets/images/top_img03.jpg" alt="麦の家ブログ">
              <figcaption class="d-top-photoText">麦の家ブログ</figcaption>
            </a>
          </figure>
          <figure class="d-top-photoItem">
            <a href="<?php echo esc_url(home_url('/')); ?>recruit">
              <img src="assets/images/top_img04.jpg" alt="採用情報">
              <figcaption class="d-top-photoText">採用情報</figcaption>
            </a>
          </figure>
        </div><!-- /.d-top-photo -->
      </div><!-- /.d-top-left -->

      <div class="d-top-right">
        <?php
        $args = array(
          'posts_per_page' => 1,
          'category_name' => 'blog',
        );
        $blog_posts = get_posts($args);
        foreach ($blog_posts as $post) : setup_postdata($_post);
        ?>
          <section class="d-top-blog">
            <a href="<?php the_permalink(); ?>">
              <h2 class="d-top-blogTitle">麦の家ブログ</h2>
              <?php the_post_thumbnail('blog', array('class' => 'd-top-blogImg'));  ?>
              <p class="d-top-blogText"><?php the_excerpt(); ?></p>
              <p class="d-top-blogLink">くわしくはこちら</p>
            </a>
          </section><!-- /.d-top-blog -->
        <?php
        endforeach;
        wp_reset_postdata();
        ?>
        <section class="d-top-policy">
          <h2 class="d-top-policyTitle">麦の家 基本方針</h2>
          <p class="d-top-policyText">私たちは<br>「人としての平等の権利」を守ります。<br>「安全で安心できる生活」の提供に努めます。<br>「自立に向けた張りのある生活」を支援します。<br>施設の機能を十分に発揮し「社会貢献」に努めます。</p>
        </section><!-- /.d-top-policy -->
        <div class="d-top-access">
          <h2 class="d-top-accessTitle">アクセス</h2>
          <div class="d-top-accessMap">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d405.65329109975767!2d138.6006411848971!3d35.572805147197776!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x601be4d50686d703%3A0xe3bb4a75b370cdd0!2z44CSNDAwLTE1MDMg5bGx5qKo55yM55Sy5bqc5biC5b-D57WM5a-655S677yU77yZ77yQ4oiS77yRIOekvuS8muemj-elieazleS6uuWPi-Wlveemj-elieS8mui6q-S9k-manOWus-iAheeZguitt-aWveiorem6puOBruWutg!5e0!3m2!1sja!2sjp!4v1535372180797" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
          <div class="d-top-accessInfo">
            <p class="d-top-accessText">
              〒400-1503<span class="d-top-accessTextBreak"></span>山梨県甲府市心経寺町490-1
            </p>
            <p class="d-top-accessText">
              ・甲府南インターから車で7分<br>
              ・甲府駅から車で25分
            </p>
            <div class="d-top-accessButtonWrap">
              <a href="https://goo.gl/maps/FV8MPcp9u4Q2" class="d-top-accessButton" target="_blank">Google Mapで見る</a>
            </div>
            <dl class="d-top-accessList">
              <dt class="d-top-accessItemTitle">TEL</dt>
              <dd class="d-top-accessItemText">055－266－3976</dd>
              <dt class="d-top-accessItemTitle">FAX</dt>
              <dd class="d-top-accessItemText">055－266－3698</dd>
            </dl>
          </div>

        </div><!-- /.d-top-access -->
      </div><!-- /.d-top-right -->
    </div><!-- /.d-top-inner -->
  </main><!-- /.d-top-main -->

  <section class="d-top-room">
    <div class="d-top-roomInfo">
      <h2 class="d-top-roomTitle">
        <span class="d-top-roomTitle d-top-roomTitle--small">相談支援事業所</span>
        麦の家相談室
      </h2>
      <p class="d-top-roomText">障害のある方が、福祉施設などを利用する際に必要な計画書の作成や、福祉や生活に関わる基本的な相談をお受けします。<br>どんなに障害が重くても地域で暮らせるよう、困っていることや悩みを一緒に考える相談支援を行っています。まずは甲府市へお問い合わせください。</p>
      <div class="d-top-roomTel">
        <p class="d-top-roomTelTitle">甲府市役所障害福祉課</p>
        <span class="d-top-roomTelNumber">055-237-5240</span>
      </div>
    </div>
    <img src="assets/images/top_img05.jpg" alt="相談支援事業所 麦の家相談室" class="d-top-roomImg">
  </section><!-- /.d-top-room -->

<?php get_footer(); ?>

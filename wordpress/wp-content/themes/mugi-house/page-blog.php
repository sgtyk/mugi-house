<?php get_header(); ?>

  <main class="c-subPage-main">
    <h2 class="c-subPage-title c-subPage-title--blog">麦の家ブログ</h2>
    <p class="c-subPage-text">今日は昨日と違う一日だから、<br class="u-pc-none">毎日たいせつに生きていく。</p>

    <?php
    $paged = get_query_var('paged');

    $args = array(
      'paged' => $paged,
      'posts_per_page' => 5,
      'category_name' => 'blog',
    );
    $news_posts = new WP_Query($args);
    while  ($news_posts->have_posts()) : $news_posts->the_post();
      $category = get_the_category();
      $cat_name = $category[0]->cat_name;
    ?>
      <section class="d-blog-post">
        <h3 class="d-blog-postTitle"><?php the_title(); ?></h3>
        <time class="d-blog-postDate"><?php the_date('Y/m/d'); ?></time>
        <div class="d-blog-postContent">
          <?php the_post_thumbnail('blog');  ?>
          <?php the_content(); ?>
        </div>
      </section><!-- /.d-blog-post -->
    <?php
    endwhile;
    if(function_exists('wp_pagenavi')):
      wp_pagenavi(array('query'=>$news_posts));
    endif;
    wp_reset_postdata();
    ?>
  </main><!-- /.d-blog-main -->

<?php get_footer(); ?>

<?php get_header(); ?>

  <main class="c-subPage-main">
    <h2 class="c-subPage-title c-subPage-title--document">保護者さま専用</h2>

    <?php if (have_posts()):  
      if( post_password_required( $post->ID ) ):?>
        <?php
        while ( have_posts() ) :
          the_post();
          remove_filter('the_content', 'wpautop');
          the_content();
        endwhile;
         // End of the loop.
        ?>
      <?php else: ?>
        <section class="d-document-section">
          <h2 class="d-document-title">資料ダウンロード</h2>
          <ol class="d-document-list">
            <?php
            if( have_rows('document_item') ):
              while ( have_rows('document_item') ) : the_row();
              $date = get_sub_field('document_date');
              $pdf = get_sub_field('document_pdf');
              $title = $pdf['title'];
              $url = $pdf['url'];
            ?>
              <li class="d-document-item">
                <a href="<?php echo $url; ?>" class="d-document-anchor" target="_blank">
                <date class="d-document-date"><?php echo $date; ?></date>
                <span class="d-document-tag">PDF</span>
                <p class="d-document-text"><?php echo $title; ?></p>
                </a>
              </li>
            <?php
              endwhile;
            endif;
            ?>
          </ol><!-- /.d-document-list. -->
        </section><!-- /.d-document- -->
      <?php endif; ?>
    <?php endif; ?>
  </main><!-- /.d-document-main -->

<?php get_footer(); ?>

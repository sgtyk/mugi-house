<?php get_header(); ?>

  <main class="c-subPage-main">
    <h2 class="c-subPage-title c-subPage-title--facility">施設紹介</h2>
    <p class="c-subPage-text">「おいしい！」「うれしい！」「楽しい！」は普段の暮らしから。</p>
    <div class="c-subPage-mainImg">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/main_facility.jpg" alt="やりたいことを「やってみる」にする場所。">
    </div>
    <p class="c-subPage-capton">
      麦の家では、日々の生活を快適に過ごすための工夫を凝らしております。<br>食事は、地域の食材を使用した給食を施設内で手作りし、食事を飲み込む力を鍛えるための、<br>嚥下の訓練なども同時に行っています。<br>居室はプライバシーを守り、安心して過ごせるよう配慮をしています。
    </p>

    <section class="c-section">
      <h3 class="c-section-title">概要</h3>
      <div class="c-contentList">
        <dl class="c-contentList-item">
          <dt class="c-contentList-itemTitle">定　　員</dt>
          <dd class="c-contentList-itemText">生活介護　50名　/　施設入所支援　50名　/　短期入所空床型　2名</dd>
        </dl>
        <dl class="c-contentList-item">
          <dt class="c-contentList-itemTitle">敷地面積</dt>
          <dd class="c-contentList-itemText">4,520㎡</dd>
        </dl>
        <dl class="c-contentList-item">
          <dt class="c-contentList-itemTitle">建物構造</dt>
          <dd class="c-contentList-itemText">鉄筋コンクリート造・２階建</dd>
        </dl>
        <dl class="c-contentList-item">
          <dt class="c-contentList-itemTitle">延床面積</dt>
          <dd class="c-contentList-itemText">1,436.99 ㎡</dd>
        </dl>
      </div>
    </section><!-- /.c-section -->

    <section class="c-section">
      <h3 class="c-section-title">施設設備</h3>
      <div class="c-section-content">
        <div class="d-facility-contentInner">
          <div class="d-facility-contentImg">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img01.jpg" alt="居室の一例">
            <div class="d-facility-contentImgRow">
              <figure class="d-facility-contentImg--sm">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img02.jpg" alt="居室の一例">
                <figcaption class="d-facility-contentImgText">居室の一例</figcaption>
              </figure>
              <figure class="d-facility-contentImg--sm">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img03.jpg" alt="居室の一例">
                <figcaption class="d-facility-contentImgText">居室の一例</figcaption>
              </figure>
            </div>
          </div>
          <div class="d-facility-contentInfo">
            <p class="d-facility-contentTitle">居室</p>
            <div class="d-facility-contentText">
              <div class="d-facility-contentRoom">
                <p class="d-facility-contentRoomName">
                  2人部屋　　1室
                </p>
                <p class="d-facility-contentRoomInfo">
                  22.10 ㎡
                </p>
              </div>
              <div class="d-facility-contentRoom">
                <p class="d-facility-contentRoomName">
                  4人部屋　 12室
                </p>
                <p class="d-facility-contentRoomInfo">
                  35.10 ㎡ ×10室<br>
                  34.43 ㎡ × 1室<br>
                  36.45 ㎡ × 1室
                </p>
              </div>
              <ul class="d-facility-contentNote">
                <li>スペースについてはカーテンで仕切られた範囲とさせていただきます。</li>
                <li>
                  居室は利用者の心身の状況や居室の空き状況により決定及び変更をさせて頂いております。都合によりご希望に沿えない場合もあります。
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div><!-- /.d-facility-content -->
      <div class="c-section-content">
        <div class="d-facility-contentInner">
          <div class="d-facility-contentImg">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img04.jpg" alt="浴室">
          </div>
          <div class="d-facility-contentInfo">
            <p class="d-facility-contentTitle">浴室</p>
            <p class="d-facility-contentText">
              特殊浴槽と一般浴槽、また天井走行式リフトにより、広くゆったりとした浴室で週に２回、快適な入浴介助を行っています。
            </p>
          </div>
        </div>
      </div><!-- /.d-facility-content -->
      <div class="c-section-content">
        <div class="d-facility-contentInner">
          <div class="d-facility-contentImg">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img05.jpg" alt="集会室 ／ 食堂">
          </div>
          <div class="d-facility-contentInfo">
            <p class="d-facility-contentTitle">集会室 ／ 食堂</p>
            <p class="d-facility-contentText">
              生活介護における日中活動の場として、また食事の提供をさせていただく場となっています。大きな窓の向こうに山々の緑が見える、開放的で気持ちのいい空間です。
            </p>
          </div>
        </div>
      </div><!-- /.d-facility-content -->
      <div class="c-section-content">
        <div class="d-facility-contentInner">
          <div class="d-facility-contentImg">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img06.jpg" alt="コミュニティールーム">
          </div>
          <div class="d-facility-contentInfo">
            <p class="d-facility-contentTitle">コミュニティールーム</p>
            <p class="d-facility-contentText">
              常勤の理学療法士がリハビリテーションを提供しています。<br>またインターネットの導入により、生活の幅が大きく広がっています。
            </p>
          </div>
        </div>
      </div><!-- /.d-facility-content -->
      <div class="c-section-content">
        <div class="d-facility-contentInner">
          <div class="d-facility-contentImg">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img07.jpg" alt="給食室">
          </div>
          <div class="d-facility-contentInfo">
            <p class="d-facility-contentTitle">給食室</p>
            <p class="d-facility-contentText">
              食事はすべて麦の家の厨房で作っているため、調理員が直接利用者の声を聞き、より良い食事提供を目指していくことができます。朝食ではパンかご飯を選択、昼食は、個人献立や行事食など、バラエティー豊かな食事を楽しんでいただくことができます。
            </p>
          </div>
        </div>
      </div><!-- /.d-facility-content -->
      <div class="c-section-content">
        <h3 class="d-facility-contentTitle">その他</h3>
        <div class="d-facility-contentImgList">
          <figure class="d-facility-contentFigure">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img08.jpg" alt="給食の一例">
            <figcaption class="d-facility-contentFigureText">給食の一例</figcaption>
          </figure>
          <figure class="d-facility-contentFigure">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img09.jpg" alt="洗面台">
            <figcaption class="d-facility-contentFigureText">洗面台</figcaption>
          </figure>
          <figure class="d-facility-contentFigure">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img10.jpg" alt="医務室">
            <figcaption class="d-facility-contentFigureText">医務室</figcaption>
          </figure>
          <figure class="d-facility-contentFigure">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility_img11.jpg" alt="ケアステーション">
            <figcaption class="d-facility-contentFigureText">ケアステーション</figcaption>
          </figure>
        </div>
      </div>
    </section><!-- /.c-section -->
  </main><!-- /.c-subPage-main -->

<?php get_footer(); ?>


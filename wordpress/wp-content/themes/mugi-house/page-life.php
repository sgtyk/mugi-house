<?php get_header(); ?>

<main class="c-subPage-main">
  <h2 class="c-subPage-title c-subPage-title--life">麦の家の生活</h2>
  <p class="c-subPage-text">やりたいことを<br class="u-pc-none">「やってみる」にする場所。</p>
  <div class="c-subPage-mainImg">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/main_life.jpg" alt="やりたいことを「やってみる」にする場所。">
  </div>
  <p class="c-subPage-capton" class="c-subPage-mainImg">
    麦の家は、日常生活において支援及び治療を必要とし、<br class="u-sp-none">居住区の市区町村より障害福祉サービス受給者証の交付を受けている方が、ご利用いただける障害者支援施設です。
  </p>
  <p class="c-subPage-capton">
    当施設では、障害の重さにかかわらず、誰もが生きることを楽しむ、手助けをしたいと考えています。<br>みんなで楽しむレクリエーションから個人の趣味、旅行やイベントの参加など、<br class="u-sp-none">できないと諦めるのではなく「やってみる」を支援します。
  </p>

  <section class="c-section">
    <h3 class="c-section-title">施設入所</h3>
    <div class="c-section-content d-life-content">
      <div class="d-life-service">
        <div class="d-life-info">
          <div class="d-life-infoTitle d-life-infoTitle--yellow">
            生活介護
            <p class="d-life-infoTitle--sm">日中のサービス</p>
          </div>
          <p class="d-life-infoText">
            食事・入浴・排泄などの基本的な介護を中心に、調理・洗濯・掃除などの家事支援、日常生活に関する相談や助言、創作活動や生産活動の機会の提供など、社会参加、自立の促進、生活の改善、身体機能の維持向上を目指したサービスを行っています。<br>また、入浴や排泄などは<strong>同性介護を行う</strong>など、プライバシーにも配慮をした対応に努めています。
          </p>
        </div>
      </div><!-- /.d-life-service -->

      <div class="d-life-service d-life-service--border">
        <div class="d-life-info">
          <div class="d-life-infoTitle d-life-infoTitle--pink">
            日中の活動
          </div>
          <p class="d-life-infoText">
            歌ったり踊ったりと体全体で音楽を感じる<strong>「音楽療法」</strong>、好きな花を選んで作品を作る<strong>「押し花」</strong>、精油や芳香浴など気分に合わせて香りを楽しむ<strong>「アロマテラピー」</strong>など、外部講師を招いて、心身ともに穏やかに、日々の暮らしが豊かになる活動を行っています。<br>
            他にも、記念式典等で発表も行う<strong>影絵</strong>、四季を感じる<strong>絵画装飾</strong>、<strong>ドミノ</strong>や<strong>お化粧</strong>など枠にはまることなく個人の活動を楽しむ<strong>「大人ちゃれんじ」</strong>、体を動かす<strong>レクリエーションズ</strong>など、すべての利用者が日中活動を楽しむことができるように、ニーズに合わせた活動の提供に努めています。<br>
            また、春と秋の<strong>前庭昼食会</strong>、<strong>夏の夕涼み会</strong>、<strong>忘年会</strong>などの季節のイベントの他、<strong>一泊旅行</strong>や<strong>日帰り旅行</strong>など、本人の希望をもとに外出ができるよう支援もしています。
          </p>
        </div>
      </div><!-- /.d-life-service -->

      <div class="d-life-serviceImgList">
        <figure class="d-life-serviceImgItem">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/life_img01.jpg" alt="音楽療法">
          <figcaption class="d-life-serviceImgText">音楽療法</figcaption>
        </figure>
        <figure class="d-life-serviceImgItem">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/life_img02.jpg" alt="絵画装飾">
          <figcaption class="d-life-serviceImgText">絵画装飾</figcaption>
        </figure>
        <figure class="d-life-serviceImgItem">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/life_img03.jpg" alt="一泊旅行">
          <figcaption class="d-life-serviceImgText">一泊旅行</figcaption>
        </figure>
      </div><!-- /.d-life-serviceImgList -->

      <div class="d-life-service d-life-service--border">
        <div class="d-life-info">
          <div class="d-life-infoTitle d-life-infoTitle--blue">
            施設入所支援
            <p class="d-life-infoTitle--sm">夜間のサービス</p>
          </div>
          <p class="d-life-infoText">
            主に夜間において、食事・入浴・排泄などの介護、生活等に関する相談・助言のほか、必要な日常生活上の支援を行います。上記の日中による支援とあわせて、一体的な支援を行います。
          </p>
        </div>
      </div><!-- /.d-life-service -->

      <div class="d-life-supporter">
        <div class="d-life-supporterTitle">
          お世話になっているみなさま
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/life_img04.png" alt="お世話になっているみなさま">
        </div>
        <ul class="d-life-supporterList">
          <li class="d-life-supporterItem">
            アーバントップ様（ヘアーカット）
          </li>
          <li class="d-life-supporterItem">
            立正佼成会様（ダンボール・ミックスペーパー等回収）
          </li>
          <li class="d-life-supporterItem">
            甲府舞鶴ライオンズクラブ様（剪定）
          </li>
          <li class="d-life-supporterItem">
            鈴かけの会様（大正琴の演奏会）
          </li>
          <li class="d-life-supporterItem">
            アンサンブルゆりかご様（コーラス）
          </li>
        </ul>
      </div><!-- /.d-life-supporter -->
    </div><!-- /.c-section-content d-life-content -->
    <a href="<?php echo get_field('life_document1', $post->ID); ?>" class="d-life-button" target="_blank">
      入所重要事項説明ダウンロード</a>
  </section><!-- /.c-section -->

  <section class="c-section">
    <h3 class="c-section-title">短期入所（空床型）</h3>
    <p class="c-section-text">
      在宅で生活されている障害のある方を対象とし、介護されている方が様々な理由で在宅での介護が困難になった際、一時的に施設を利用することができるサービスです。麦の家では空床利用型となります。
    </p>
    <a href="<?php echo get_field('life_document2', $post->ID); ?>" class="d-life-button" target="_blank">短期入所詳細ダウンロード</a>
  </section><!-- /.c-section -->
</main><!-- /.c-subPage-main -->

<?php get_footer(); ?>

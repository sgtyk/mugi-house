<?php get_header(); ?>

  <main class="c-subPage-main">
    <h2 class="c-subPage-title c-subPage-title--policy">大事にしていること</h2>
    <section class="d-policy-message">
      <figure class="d-policy-messagePhoto">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy_img01.png" alt="">
        <figcaption class="d-policy-messageName">長岡均施設長</figcaption>
      </figure>
      <div class="d-policy-messageInfo">
        <h3 class="d-policy-messageTitle">麦の家は、開設以来<br class="u-pc-none">「自己選択」<br class="u-sp-none">「自己決定」ということを大切にしてきました。</h3>
        <p class="d-policy-messageText">麦の家では、すべての利用者が主役であり、やりたいことを自分で選び、決めることができます。障害の重さにかかわらず、選ぶという権利は、本来は他人から制限を受けるものではないのです。</p>
        <p class="d-policy-messageText">ただ、心身の障害により、利用者自身にはできないことがあります。その部分を支援し、安全を第一に考えながら、可能な限り本人の希望を実現していくことが、当施設の役割だと考えています。</p>
      </div>
    </section>
    <div class="d-policy-contentWrap">
      <div class="d-policy-content">
        <figure class="d-policy-contentImg">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy_img02.jpg" alt="八景島シーパラダイスの一泊旅行">
          <figcaption class="d-policy-contentImgText">八景島シーパラダイスの一泊旅行</figcaption>
        </figure>
        <p class="d-policy-contentTitle">
          <span class="d-policy-contentTitleInner">
            自分が、<br>
          </span>
          <span class="d-policy-contentTitleInner">
            今できることをせいいっぱいやる
          </span>
        </p>
        <p class="d-policy-contentText">歩ける機能があるのに「転んでしまうから」と言って車椅子に乗ることはない。可能なかぎり自分の足で歩いてほしい。</p>
        <p class="d-policy-contentText">利用者ができることを支援するのが私たちの役割。<br>外出や旅行も利用者が希望・企画した活動を、お手伝いして実行できるようにしています。</p>
      </div>
      <div class="d-policy-content">
        <figure class="d-policy-contentImg">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy_img03.jpg" alt="甲府市立中道南小学校との交流">
          <figcaption class="d-policy-contentImgText">甲府市立中道南小学校との交流</figcaption>
        </figure>
        <p class="d-policy-contentTitle">
          <span class="d-policy-contentTitleInner">
            私たちは、利用者の方々と、<br>
          </span>
          <span class="d-policy-contentTitleInner">
            人としてのつながりを大切にしています。
          </span>
        </p>
        <p class="d-policy-contentText">楽しいことは一緒に楽しみ、悲しいことはともに受け止め、生きていくうえで経験する様々なことを、ここで感じていただけるようにと思っています。</p>
        <p class="d-policy-contentText">どんなに障害が重くても、一人ひとりを尊重し、生きることそのものに満足していただけるように努力して参ります。</p>
      </div>
    </div>

  </main><!-- /.c-subPage-main -->

<?php get_footer(); ?>


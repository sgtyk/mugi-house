<?php get_header(); ?>

  <main class="c-subPage-main">
    <h2 class="c-subPage-title c-subPage-title--recruit">採用情報</h2>
    <p class="c-subPage-text">かけがえのない笑顔に、<br class="u-pc-none">出会いたい。</p>
    <div class="c-subPage-mainImg">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/main_recruit.jpg" alt="やりたいことを「やってみる」にする場所。">
    </div>
    <p class="c-subPage-capton" class="c-subPage-mainImg">
      麦の家では利用者とともに生きることの大切さを感じ、毎日を笑顔にする仲間を随時募集しています。<br>
      お気軽にお問い合わせください。
    </p>

    <div class="d-recruit-concept">
      <div class="d-recruit-conceptRow">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit_img01.jpg" alt="" class="d-recruit-conceptImg">
        <p class="d-recruit-conceptText">
          利用者と関わるたびに、<br>その純粋でまっすぐな<br>心や笑顔に<span class="d-recruit-conceptTextInner">私自身の心が洗われる、<br>私自身も笑顔になれる、</span>そんな日々です。
        </p>
      </div>
      <div class="d-recruit-conceptRow">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit_img02.jpg" alt="" class="d-recruit-conceptImg">
        <p class="d-recruit-conceptText">
          <span class="d-recruit-conceptTextInner">ここでは毎日が変化していて、</span>楽しいこと、<br class="u-sp-none">大変なこと、様々なことが起こりますが、<br class="u-sp-none"><span class="d-recruit-conceptTextInner">どれも新鮮</span>です。
        </p>
      </div>
      <div class="d-recruit-conceptRow">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit_img03.jpg" alt="" class="d-recruit-conceptImg">
        <p class="d-recruit-conceptText">
          この仕事に携わって、<br><span class="d-recruit-conceptTextInner">当たり前に明日がやってくるという考えは<br>違うこと</span>に気づきました。
        </p>
      </div>
    </div>


    <section class="c-section">
      <h3 class="c-section-title">募集要項</h3>
      <?php
      if( have_rows('recruit_item') ):
        while ( have_rows('recruit_item') ) : the_row();
      ?>
        <div class="c-contentList">
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">募集職種</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field1'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">資格</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field8'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">給与</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field2'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">諸手当</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field3'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">昇給</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field4'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">賞与</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field5'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">勤務時間</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field9'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">休日等</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field6'); ?>
            </dd>
          </dl>
          <dl class="c-contentList-item c-contentList-item--spNoneFlex">
            <dt class="c-contentList-itemTitle">その他</dt>
            <dd class="c-contentList-itemText">
              <?php the_sub_field('recruit_field7'); ?>
            </dd>
          </dl>
        </div><!-- /.c-contentList -->
      <?php
        endwhile;
      endif;
      ?>
    </section><!-- /.c-section -->
  </main><!-- /.c-subPage-main -->

<?php get_footer(); ?>

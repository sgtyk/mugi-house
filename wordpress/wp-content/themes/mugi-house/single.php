<?php get_header(); ?>
  <main class="c-subPage-main">
    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <section class="d-blog-post">
        <h3 class="d-blog-postTitle"><?php the_title(); ?></h3>
        <time class="d-blog-postDate"><?php the_date('Y/m/d'); ?></time>
        <div class="d-blog-postContent">
          <?php the_post_thumbnail('blog');  ?>
          <?php the_content(); ?>
        </div>
      </section><!-- /.d-blog-post -->
    <?php endwhile; endif; ?>
  </main><!-- /.d-blog-main -->

<?php get_footer(); ?>
